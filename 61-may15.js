var Animal = function (color) {
  this.color = color;
};

Animal.prototype.breathe = function () {
  return "breathe";
};

var a = new Animal("blue");
a.breathe();
// a.__proto__ => Animal.prototype

var Cat = function (name, color) {
  this // => new blank object
  Animal.call(this, color);
  this.name = name;
};

// Cat.prototype = Animal.prototype; // WRONG - modifies Animal prototype

// Cat.prototype = {}; // starts out this way
// Cat.prototype = new Animal("green"); // right inheritance chain, but have to instantiate a new Animal

var Surrogate = function () {};
Surrogate.prototype = Animal.prototype;
Cat.prototype = new Surrogate(); //=> {}.__proto__ = Animal.prototype;

// Cat.prototype = Object.create(Animal.prototype);



var Fish = function () {};
Surrogate.prototype = Fish.prototype;
Fish.prototype.swim = function () {
  return "swim";
};

Function.prototype.inherits = function (parentClass) {

};

Cat.inherits(Animal);

Cat.prototype.pur = function () {
  return "pur";
};

var c = new Cat("Sennacy");
c.pur();
// c.__proto__ => Cat.prototype



// c => Cat.prototype => Animal.prototype => Object.prototype




var addTwoNums = function (a, b) {
  return "I am " + this.name + " and the sum is " + (a + b);
};

var cat = {
  name: "Sennacy"
};

var addToOne = addTwoNums.bind(cat, 1);
addToOne(2); // 3



function a(first, second) {
  arguments // => keyword that captures all the arguments in an object
  // NOT AN ARRAY ^

  console.log(arguments, 'original args');
  var arrayArgs = Array.prototype.slice.call(arguments);
  console.log(arrayArgs, 'arrayArgs');
};

function b(first, second) {

}



var dumbArray = {
  "0": "a",
  "1": "b",
  "2": "c",
  length: 3
};


// => related interview questions: look up 'duck-typing' / 'polymorphism'
