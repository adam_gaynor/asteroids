"use strict";

function sum () {
  var args = Array.prototype.slice.call(arguments);
  var sum = 0;

  args.forEach(function(num) {
    sum += num;
  });

  return sum;
};

Function.prototype.myBind = function (boundThing) {
  var fn = this;
  var args = Array.prototype.slice.call(arguments, 1);

  return function () {
    var innerArgs = Array.prototype.slice.call(arguments);
    return fn.apply(boundThing, args.concat(innerArgs));
  };
};
function Cat(name) {
  this.name = name;
};

Cat.prototype.says = function (sound) {
  console.log(this.name + " says " + sound + "!");
}

function curriedSum(numArgs) {
  var numbers = [];

  function _curriedSum(num) {
    numbers.push(num);

    if (numbers.length === numArgs) {
      var sum = 0;
      for (var i = 0; i < numbers.length; i++) {
        sum += numbers[i];
      }
      return sum;
    } else {
      return _curriedSum;
    }
  }
  return _curriedSum;
};

Function.prototype.curry  = function(numArgs) {
  var args = [];
  var fn = this;
  function _curry(arg) {
    args.push(arg);

    if (args.length === numArgs) {
      return fn.apply(this, args);
    } else {
      return _curry;
    }
  }
  return _curry;
};


var addTwoNums = function (a, b) {
  return "The sum is " + (a + b);
};
