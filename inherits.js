"use strict";

Function.prototype.inherits = function(parentClass) {
  var Surrogate = function() {};
  Surrogate.prototype = parentClass.prototype;
  this.prototype = new Surrogate();
}

var MovingObject = function(name) {
  this.name = name;
}

MovingObject.prototype.move = function() {
  console.log(this.name + " moved.");
}

var Asteroid = function(name) {
  MovingObject.call(this, name);
}

Asteroid.inherits(MovingObject);

var a = new Asteroid("Charlie");
a.move();
