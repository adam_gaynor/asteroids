"use strict";

(function () {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var DIM_X = 700;
  var DIM_Y = 700;
  var NUM_ASTEROIDS = 15;

  var Game = Asteroids.Game = function() {
    this.asteroids = [];
    this.addAsteroids();
    this.dimX = DIM_X;
    this.dimY = DIM_Y;
    this.ship = new Asteroids.Ship({pos: this.randomPosition(), game: this});
    this.allObjects = this.allObjects();
  }

  Game.prototype.addAsteroids = function() {
    //get a random position
    //new asteroid at that position
    for (var i = 0; i < NUM_ASTEROIDS; i++) {
      var position = this.randomPosition();
      this.asteroids.push(new Asteroids.Asteroid({ pos: position, game: this }));
    }
  }

  Game.prototype.randomPosition = function() {
    //generate a random position
    var x_Dimension = Math.random() * DIM_X;
    var y_Dimension = Math.random() * DIM_Y;

    return [x_Dimension, y_Dimension];
  }

  Game.prototype.draw = function (ctx) {
    ctx.clearRect(0, 0, DIM_X, DIM_Y);

    this.allObjects.forEach(function (object) {
      object.draw(ctx);
    });
  }

  Game.prototype.moveObjects = function () {
    this.allObjects.forEach(function (object) {
      object.move();
    });
  }

  Game.prototype.step = function () {
    this.moveObjects();
    this.checkCollisions();
  };

  Game.prototype.wrap = function (pos) {
    var x_dim = pos[0];
    var y_dim = pos[1];
    var z = this.asteroids[0].radius + 10;

    x_dim = (x_dim > DIM_X + z) ? -z : x_dim;
    x_dim = (x_dim < -z) ? DIM_X + z : x_dim;
    y_dim = (y_dim > DIM_Y + z) ? -z : y_dim;
    y_dim = (y_dim < -z) ? DIM_Y + z : y_dim;

    return [x_dim, y_dim];
  };

  Game.prototype.checkCollisions = function () {
    for (var i = 0; i < this.allObjects.length - 1; i++) {
      for (var j = i + 1; j < this.allObjects.length; j++) {
        if (this.allObjects[i].isCollidedWith(this.allObjects[j])) {
          this.allObjects[i].collideWith(this.allObjects[j]);
        };
      }
    };
  };

  Game.prototype.remove = function (asteroid) {
    var i = this.asteroids.indexOf(asteroid);
    this.asteroids.splice(i, 1);
  };

  Game.prototype.allObjects = function () {
    return this.asteroids.concat([this.ship]);
  }

}) ();
