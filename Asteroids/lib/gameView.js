"use strict";

(function () {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var GameView = Asteroids.GameView = function (game, ctx) {
    this.game = game;
    this.ctx = ctx;
  };

  GameView.prototype.start = function () {
    window.setInterval((function () {
      this.game.step();
      this.game.draw(this.ctx);
    }).bind(this), 20);
    this.bindKeyHandlers();
  };

  GameView.prototype.bindKeyHandlers = function () {
    key('up', function() {
      this.game.ship.power([0,1]);
      });
    key('down', function() {
      this.game.ship.power([0,-1]);
     });
    key('left', function() {
      this.game.ship.power([-1,0]);
    });
    key('right', function() {
      this.game.ship.power([1,0]);
    });
  }

}) ();
