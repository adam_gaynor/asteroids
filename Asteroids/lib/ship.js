"use strict";

(function () {
  if (typeof Asteroids === "undefined") {
    window.Asteroids = {};
  }

  var COLOR = "#1d99d4";
  var RADIUS = 15;

  var Ship = Asteroids.Ship = function (options) {
    Asteroids.MovingObject.call(
      this,
      {
        pos: options.pos,
        vel: [0,0],
        radius: RADIUS,
        color: COLOR,
        game: options.game
      }
    );
  };

  Asteroids.Util.inherits(Ship, Asteroids.MovingObject);

  Ship.prototype.power = function (impulse) {
    this.vel[0] += impulse[0];
    this.vel[1] += impulse[1];
  }

  Ship.prototype.relocate = function () {
    this.pos = this.game.randomPosition();
    this.vel = [0, 0];
  };

}) ();
